from setuptools import setup
from setuptools.extension import Extension

from Cython.Build import cythonize
from Cython.Distutils import build_ext
import os
import shutil

folder_list = ("parabole-zk-system-manager", "parabole-learning-starter", "parabole-auto-ontology", "parabole-bert-supervised",
"parabole-bert-validator", "parabole-conll-generator-spacy", "parabole-corpus-document-filter", "parabole-document-reader",
"parabole-google-corpus-downloader", "parabole-analysis-model-generator", "parabole-py-rdf-creation", "parabole-sentence-ranking",
"parabole-structure-extractor", "parabole-term-filter", "parabole-user-review", "parabole-wordspace-generation-learning-pipeline",
"parabole-wordspace-validation")
cwd = os.getcwd()
folder_list1 = ["parabole-learning-starter", "parabole-term-filter"]
name1="parabole-secret-encryption"
for folder_name in folder_list1:
    os.chdir(f"{cwd}/{folder_name}")
    to_be_deleted = []
    for file1 in os.listdir(f"../{folder_name}"):
        exclude_files = ["config", "driver", "Driver", "cfg", "_init"]
        if file1[-3:] == ".py":
            if any(x in file1 for x in exclude_files):
                print(os.getcwd())
                print("Driver or Config file excluding for Encryption ",file1)
            else
                try:
                    setup(
                        name=name1,
                        ext_modules=cythonize(
                            Extension(file1[:-3], [file1]),
                            #Extension(file1[:-3]),
                            build_dir="build",
                            compiler_directives={'language_level': "3", 'always_allow_keywords': True}),
                        cmdclass=dict(build_ext=build_ext),
                        packages=[]
                    )
                    to_be_deleted.append(file1)
                except Exception as e:
                    print(e)
                    print("Cannot cythonise {}".format(file1))

    for file2 in to_be_deleted:
        print("Deleting file --> {}".format(file2))
        os.remove(f"{cwd}/{folder_name}/{file2}")
    print("Deleting Build folder")
    os.chdir(cwd)
    try:
        shutil.rmtree(f"{cwd}/{folder_name}/build")
    except Exception as e:
        print(e)
        print("Build folder Doesn't Exists for ", folder_name)

