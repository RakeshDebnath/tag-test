FROM python:3.7.5
LABEL Rakesh Debnath <rakesh.debnath@parabole.ai>

# for package configuration
WORKDIR /usr/src/cache

# copy files to container
COPY . /app
WORKDIR /app

# installing python modules
RUN cat requirements.txt | xargs -n 1 python3.7 -m pip install

ENTRYPOINT ["python", "term_filter_driver.py"]
