#!/bin/bash
WORKING_DIR=$(pwd)
#global variable declaration
AzCopy=$WORKING_DIR/azcopy
DIR="$WORKING_DIR/env"
DIR1="$WORKING_DIR/kafka_2.11-2.3.0"
DIR2="$WORKING_DIR/solr-8.3.0"
KAFKA_PATH=$WORKING_DIR/kafka_2.11-2.3.0
LOG_DIR="$WORKING_DIR/logs"
DEPLOYMENT_SCRIPT_LOG=$WORKING_DIR/deployment_script_validation.txt

git config --global --unset-all user.email
git config --global --unset-all user.password
git config --global --unset-all credential.helper
user_name="paraboleadmin"
user_email="repo.manager@parabole.ai"
user_pwd="ATBBk9CahUPRw9tzaJRh8vJcf7hM93E51DFA"
url="bitbucket.org/parabole-train/"
git config --global user.email $user_email
git config --global user.password $user_pwd
git config --global credential.helper 'cache --timeout=7200'

#removing old scripts: files: folder for duplicate use
rm -rf $DEPLOYMENT_SCRIPT_LOG parabole-bert-supervised

name="phase3"
#user input for branch selection and global git configuration
user_input_for_branch_selection(){
	read -p "Enter Branch or Tag Name : " name
  	echo -e "\e[1;34m\nSelected Branch --> \e[1;33m$name \e[0m"
  	echo -e "\e[1;34mSelected Branch --> \e[1;33m$name \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
  	echo -e "\e[1;34mPipeline Installation Directory selected as -> \e[1;33m$WORKING_DIR \e[0m"
  	echo -e "\e[1;34mPipeline Installation Directory selected as $WORKING_DIR \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
}
#Script execution trigger print
pipeline_update="false"
script_execution_trigger_print(){
  	if [ -d "$DIR1" ] || [ -d "$DIR2" ]; then
		pipeline_update="true"
        	echo -e "\e[1;34mScript Execution --> \e[1;33mUpdate Existing Pipeline\e[0m"
        	echo -e "\e[1;34mScript Execution --> \e[1;33mUpdate Existing Pipeline\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
            echo
 	else
        	echo -e "\e[1;34mScript Execution --> \e[1;33mNew Deployment\e[0m"
        	echo -e "\e[1;34mScript Execution --> \e[1;33mNew Deployment\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
            echo
  	fi
}
#Deployment level default configs
train_default_configs(){
if [ $pipeline_update == "true" ]; then
        read -p "Do you want to keep the existing train_env.txt? [Options : y/n ; default : y] : " -n 1 -r
        echo
        if [[ $REPLY =~ ^[Nn]$ ]]; then
		pipeline_update="false"
                echo -e "\e[1;32m Pulling latest train_env.txt in current working dir\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
        else
		pipeline_update="true"
                echo -e "\e[1;33m Existing train_env.txt retained\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
        fi
fi
}

script_execution_verification(){
	echo -e "\e[1;33m\nPlease Verify the above Details and Provide Confirmation to Continue the Script Execution \e[0m"
  	read -p "Are you sure?[y/n] " -n 1 -r
  	echo    # (optional) move to a new line
  	if [[ $REPLY =~ ^[Yy]$ ]]; then
        	echo -e "\e[1;32mUser Confirmation Done | Script execution Started\e[0m"
  	else
        	echo -e "\e[1;31mUser Interrupted | If you want to proceed with the script please press y/Y\e[0m"
        	exit
  	fi
}

#Only for server22 | adding devtoolset-7
hostname=$(hostname -I | awk '{print $1}')
if [ $hostname == "172.16.214.22" ]; then
	source scl_source enable devtoolset-7
fi


#System check
os_system_check(){
	echo -e "\e[1;33m\n Checking System Installation \e[0m"
  	if [  -n "$(uname -a | grep Ubuntu)" ]; then
		echo -e "\e[1;32m UBUNTU Release Found !!! Installing Now\n\e[0m" 
	  	echo "Installing on UBUNTU Server"  >> $DEPLOYMENT_SCRIPT_LOG
  	  	system="apt-get"
  	else
	  	echo -e "\e[1;32m RHEL Release Found !!! Installing Now \n\e[0m"
	  	echo "Installing on RHEL Server" >> $DEPLOYMENT_SCRIPT_LOG
    	 	system="yum"
  	fi
}

#python librabry dependencies
python_dependent_library(){
	echo -e "\e[1;33m Checking for Dependent System Library packages\e[0m"
  	if [ -x "$(command -v python3.10)" ] && [ "$(python3.10 --version)" == "Python 3.10.5" ]; then
		echo -e "\e[1;32m Dependent System Library packages Already Present !!! Skipping System Library Installation \n\e[0m"
	  	echo "Python3.10.5 Already Present !!! Skipping System Library Installation" >> $DEPLOYMENT_SCRIPT_LOG
  
  	#python librabry installation on ubuntu
  	elif [ $system == "apt-get" ]; then
		echo -e "\e[1;33m Installing Depended Library packages in Ubuntu \e[0m"
		sudo $system -y update  
		echo "Sqlite3 installation Started !!"
	  	sudo $system install -y libsqlite3-dev >/dev/null 2>&1
	  	sqlite3_status=$?
	  
		if [ $sqlite3_status != 0 ]; then
      			echo "Sqlite3 installation failed !! Trying apt fix broken and Reinstall!!!!"
		  	echo "Sqlite3 installation failed !! Trying apt fix broken and Reinstall!!!!" >> $DEPLOYMENT_SCRIPT_LOG
   	  		sudo $system --fix-broken install >/dev/null 2>&1
		  	sudo $system install -y libsqlite3-dev >/dev/null 2>&1
		  	sqlite3_status_retry=$?
      			
			if [ $sqlite3_status_retry != 0 ]; then
				echo "Sqlite3 Re-installation failed!!!! Terminating Clone script" >> $DEPLOYMENT_SCRIPT_LOG
			  	echo -e "\e[1;31m libsqlite3-dev installation failed !! Please reach out System Administrator(manoj/rakesh) \e[0m"
			  	echo "Failed to Install Parabole Train Pipeline"
			  	exit
		  	else
        			echo -e "\e[1;32m Sqlite3 installation successfull \e[0m" 
        			echo -e "\e[1;32m Sqlite3 installation successfull \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
     	 		fi
    		else
      			echo -e "\e[1;32m Sqlite3 installation successfull \e[0m"
      			echo -e "\e[1;32m Sqlite3 installation successfull \e[0m"		>> $DEPLOYMENT_SCRIPT_LOG
	  	fi

	  	#installing ubuntu apt packages
	  	echo -e "\e[1;33m Python3.10.5 dependent library installation started \e[0m"
    		sudo $system install -y make build-essential libssl-dev libpq-dev zlib1g-dev software-properties-common \
       		libbz2-dev libreadline-dev wget curl llvm \
       		libncurses5-dev libncursesw5-dev xz-utils tk-dev liblzma-dev unzip >/dev/null 2>&1	  
    		python_library=$?
	  
		if [ $python_library != 0 ]; then
      			echo -e "\e[1;31mSome of the Python Library Failed to install !! Contact System Administrator\e[0m"
		  	echo "Some of the Python Library Failed to install" >> $DEPLOYMENT_SCRIPT_LOG
	    		exit
    		else
	    		echo -e "\e[1;32mPython3.10.5 dependent library installation successfull\n\e[0m"
      			echo -e "\e[1;32mPython3.10.5 dependent library installation successfull\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
    		fi
    		install_python3_10
    
  	#python librabry installation on rhel
  	elif [ $system == "yum" ]; then
		echo -e "\e[1;33m \nRhel Library Installation Started \e[0m"
		sudo $system -y update  
		sudo $system -y groupinstall "Development Tools" >/dev/null 2>&1
	  	dev_tools=$?
	  
		if [ $dev_tools != 0 ]; then
      			echo -e "\e[1;31mFailed to Install Development Tools !! Contact System Administrator(manoj/rakesh)\e[0m"
			echo -e "\e[1;31mFailed to Install Development Tools !! Contact System Administrator\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
      			exit
	  	else
	    		echo -e "\e[1;32mPython Development Tools Installed Successsfully\e[0m"
      			echo -e "\e[1;32mPython Development Tools Installed Successsfully\e[0m"  >> $DEPLOYMENT_SCRIPT_LOG
	  	fi

    		sudo $system -y install gcc openssl-devel bzip2-devel libffi-devel gcc-c++ make unzip >/dev/null 2>&1
	  	other_dev_tools=$?

	        if [ $other_dev_tools != 0 ]; then
      			echo -e "\e[1;31mFailed to Install gcc opensl and others !! Contact System Administrator(manoj/rakesh)\e[0m"
      			echo -e "\e[1;31mFailed to Install gcc opensl and others !! Contact System Administrator\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
      			exit
    		else
      			echo -e "\e[1;32mPython Dependency gcc opensl and others Installed Successsfully\e[0m"
      			echo -e "\e[1;32mPython Dependency gcc opensl and others Installed Successsfully\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
    		fi
    		install_python3_10
    
  	else
		echo -e "\e[1;31mSomething Went Wrong in Library Installation Due to wrong OS selection !!! Terminating Script\e[0m"
    		echo -e "\e[1;31mSomething Went Wrong in Library Installation Due to wrong OS selection !!! Terminating Script\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
	  	exit
  	fi
}  

#Installing Python
install_python3_10(){
	echo -e "\e[1;33m Installing Python3.10.5 \e[0m"
 	sudo $system update -y >/dev/null 2>&1
  	system_update=$?
 
	if [ $system_update != 0 ]; then
    		echo " $system update failed During Python make installation"
    		echo " $system update failed During Python make installation" >> $DEPLOYMENT_SCRIPT_LOG
  	fi
 	
	wget https://www.python.org/ftp/python/3.10.5/Python-3.10.5.tgz >/dev/null 2>&1
 	tar xvf Python-3.10.5.tgz >/dev/null 2>&1
 	cd Python-3.10.5
 	./configure --enable-loadable-sqlite-extensions --enable-optimizations --with-ensurepip=install >/dev/null 2>&1
 	make -j8 build_all >/dev/null 2>&1
 	make -j8 altinstall >/dev/null 2>&1
 	cd $WORKING_DIR
  	if [ -x "$(command -v python3.10)" ] && [ "$(python3.10 --version)" == "Python 3.10.5" ]; then
    		echo -e "\e[1;32m Successfully Installed Python3.10.5 \n\e[0m"
    		echo -e "\e[1;32m Successfully Installed Python3.10.5 \n\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
  	else
    		echo -e "\e[1;31m Failed to Install Python3.10.5 !! Contact System Administrator (manoj/rakesh)\e[0m"
    		echo -e "\e[1;31m Failed to Install Python3.10.5 !! Contact System Administrator\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
    		exit
  	fi
}

#cargo env activate
activate_cargo_env(){
	echo -e "\e[1;33m Loading cargo env for python environment\e[0m"
  
	if [ ! -f "$HOME/.cargo/env" ]; then
    		echo -e "\e[1;33m Downloading cargo env\e[0m"
    		curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y >/dev/null 2>&1
  	fi
 	
	source $HOME/.cargo/env 
  	echo -e "\e[1;32m cargo env imported\n\e[0m"
}

#Cloning Dependent Scripts
pull_latest_scripts(){
	echo -e "\e[1;33m Downloading dependent Scripts \e[0m"
  	#SCRIPT_URL=https://ParaboleRepoManager@bitbucket.org/parabole-train/parabole-pipeline-scripts.git
  	SCRIPT_DIR=$WORKING_DIR/parabole-pipeline-scripts
  	
	if [ -d "$SCRIPT_DIR" ] ; then
 		echo "Script repo already exists"
   		cd $SCRIPT_DIR
   		git_username=$(git config --get remote.origin.url)
	
		if [ ${git_username:8:13} != $user_name ]; then
        		echo "Remote repo url mismatch. Deleting Script Repo"
        		cd $WORKING_DIR
        		sudo rm -rf $SCRIPT_DIR
        		script_repo="parabole-pipeline-scripts"
        		base_url2="https://$user_name:$user_pwd@$url$script_repo.git"
        		sudo git clone $base_url2 >/dev/null 2>&1
        		echo "Repo Url mismatch Solved"
   		fi
  	else
     		echo "Script Repo not present cloning now"
     		script_repo="parabole-pipeline-scripts"
     		base_url2="https://$user_name:$user_pwd@$url$script_repo.git"
     		sudo git clone $base_url2 >/dev/null 2>&1
  	fi
  
	cd $SCRIPT_DIR
  	sudo git reset --hard >/dev/null 2>&1
  	sudo git checkout $name && sudo git pull >/dev/null 2>&1
  	sudo cp -rf pipeline_restart.bash pipeline_start.bash pipeline_terminate.bash delete_zk_system_manager.sh clear_logs.sh create-requirements-file.py train_module_list_config.sh ../
  	if [ $pipeline_update == "false" ]; then
		cp -rf train_env.txt ../
	fi
	cd $WORKING_DIR
  	echo -e "\e[1;32m Script dependency Solved \n \e[0m"
}


#Installing AZCOPY
install_azcopy(){
	if test -f "$AzCopy"; then
		echo -e "\e[1;32m AzCopy already exists.\e[0m"
  	else
		echo "Installing AzCopy in...$(pwd)"
  		wget -O azcopy_v10.tar.gz https://aka.ms/downloadazcopy-v10-linux >/dev/null 2>&1
  		sudo tar -tf azcopy_v10.tar.gz >/dev/null 2>&1
  		sudo tar -xf azcopy_v10.tar.gz --strip-components 1
  		sudo rm -rf azcopy_v10.tar.gz  ThirdPartyNotice.txt NOTICE.txt
  	fi
}

#Installing Base Package
install_base_package(){
	if [ -d "$DIR1" ] || [ -d "$DIR2" ]; then
		echo -e "\e[1;32m Utilities already installed...\e[0m"
  	else
		echo -e "\e[1;34mDownloading Base_Package in...$(pwd)\e[0m"
	  	sudo $AzCopy copy 'https://parabolebasepackage.blob.core.windows.net/basepackage/util_package.zip?sp=rw&st=2023-01-23T07:02:22Z&se=2023-12-31T15:02:22Z&spr=https&sv=2021-06-08&sr=c&sig=f8KAbATOW22R%2BR%2BON7B9YfURUTs4DOGLHF1R1p8CLBQ%3D' .
  		echo "Unzipping Utilities..."
  		sudo unzip -o util_package.zip >/dev/null 2>&1
  		sudo rm -rf util_package.zip
  	fi
}

#Installing JRE
install_jre(){
  jre_path=/usr/lib/java
  if [ -x "$(command -v java)" ]; then
	  echo -e "\e[1;32m Java Already Present\e[0m \n"
  else
	if [ -d "$jre_path" ] ; then
 		  echo "Java folder exists"
   	else
  		sudo mkdir $jre_path
   	fi
   	sudo unzip -o jre1.8.0_271.zip
   	sudo mv jre1.8.0_271 /usr/lib/java
   	sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/java/jre1.8.0_271/bin/java" 1
   	sudo echo -e "\n#Java Environment \nexport JAVA_HOME=/usr/lib/java/jre1.8.0_271 \nexport PATH=\"\$PATH:\$JAVA_HOME/bin\" \n" >> ~/.bashrc
   	source ~/.bashrc
	if [ -x "$(command -v java)" ]; then
          echo -e "\e[1;32m Java Installed Successfully\e[0m \n"
	  echo -e "\e[1;32m Java Installed Successfully\e[0m \n" >> $DEPLOYMENT_SCRIPT_LOG
	  rm -rf $WORKING_DIR/jre1.8.0_271.zip
  	else
	  echo -e "\e[1;31m Failed to Install Java\e[0m \n"
	  echo -e "\e[1;31m Failed to Install Java\e[0m \n" >> $DEPLOYMENT_SCRIPT_LOG
	fi

  fi
}

#STEPS for parabole module clone/update
create_backup_env_folder(){
	sudo rm -rf $WORKING_DIR/backup_env && sudo mkdir $WORKING_DIR/backup_env && sudo chmod 777 $WORKING_DIR/backup_env
}

#Terminating existing pipeline
terminate_pipeline(){
	echo -e "\e[1;33m Stopping Pipeline for latest pool | Terminating now !!!\e[0m"
  	cd $WORKING_DIR
	./pipeline_terminate.bash; ./clear_logs.sh >/dev/null 2>&1
	cd $WORKING_DIR
	echo " "
}


#Cloning/Updating parabole module and creating env
clone_module_create_env(){
    source $WORKING_DIR/train_module_list_config.sh
	echo -e "\n\e[1;33m Parabole module cloning started \e[0m"
	clone_module(){
                        base_url="https://$user_name:$user_pwd@$url$i.git"
                        git clone $base_url >/dev/null 2>&1
                        cd $WORKING_DIR/$i && git checkout $name >/dev/null 2>&1
                        cd $WORKING_DIR

                }

	for i in "${arr[@]}"
	do
		echo -e "\e[1;35mWorking with module $i \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
  		echo -e "\e[1;35mWorking with module $i \e[0m"
		
		if [ "$i" == "parabole-pipeline-scripts" ] ; then
		echo -e "\e[1;32m Module pull ignored for $i\e[0m"
		
		elif [ -d "$i" ] ; then
			echo -e "\e[1;34m $i module already exist's !! trying to pull latest \e[0m"
    			echo -e "\e[1;34m $i module already exist's !! trying to pull latest \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
			cd $i
    			
			if [ -d "$WORKING_DIR/$i/env" ]; then
				echo -e "\e[1;32m Existing env found in $i !! backing up before git stash and pull \e[0m"
				mv  env/ ../backup_env
			fi
      			if [ -d "$WORKING_DIR/$i/causal_default" ]; then
				echo -e "\e[1;32m Existing causal model found in $i !! backing up before git stash and pull \e[0m"
				mv  causal_default/ ../backup_env
			fi
            if [ -d "$WORKING_DIR/$i/entity_default" ]; then
                                echo -e "\e[1;32m Existing entity model found in $i !! backing up before git stash and pull \e[0m"
                                mv  entity_default/ ../backup_env
                        fi

      			if [ -d "$WORKING_DIR/$i/models" ]; then
				echo -e "\e[1;32m Existing Bart model found in $i !! backing up before git stash and pull \e[0m"
				mv  models/ ../backup_env
			fi
			
			git_username=$(git config --get remote.origin.url)
			cd $WORKING_DIR
			
			if [ ${git_username:8:13} == $user_name ]; then
				cd $i
				rm -rf .idea __pycache__
				git reset --hard >/dev/null 2>&1
				git clean -fxd >/dev/null 2>&1
				git stash >/dev/null 2>&1
				branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
			
				if [[ $name != $branch ]]; then
					git fetch --all --tags >/dev/null 2>&1
					git checkout -f $name >/dev/null 2>&1
				fi
			
				git pull origin $name >/dev/null 2>&1
				cd $WORKING_DIR
			else
				echo -e "\e[1;33m $i module not cloned Using $user_name !! Deleting and Recloning now  \e[0m"
      				echo -e "\e[1;33m $i module not cloned Using $user_name !! Deleting and Recloning now  \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
				sudo rm -rf $WORKING_DIR/$i
				clone_module
			fi
    		
			cd $i
    			
			if [ -d "$WORKING_DIR/backup_env/env" ]; then
				echo -e "\e[1;32m Env restored after latest code pull in $i \e[0m"
				mv -f ../backup_env/env .
			fi
      			if [ -d "$WORKING_DIR/backup_env/causal_default" ]; then
				echo -e "\e[1;32m causal model restored after latest code pull in $i \e[0m"
				mv -f ../backup_env/causal_default .
			fi
            if [ -d "$WORKING_DIR/backup_env/entity_default" ]; then
                                echo -e "\e[1;32m entity model restored after latest code pull in $i \e[0m"
                                mv -f ../backup_env/entity_default .
                        fi

     			if [ -d "$WORKING_DIR/backup_env/models" ]; then
				echo -e "\e[1;32m Bart model restored after latest code pull in $i \e[0m"
				mv -f ../backup_env/models .
			fi
    
			cd $WORKING_DIR
			echo -e "\e[1;32m $i module pull is Successfull \e[0m"
    			echo -e "\e[1;32m $i module pull is Successfull \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
		
		else
			echo -e "\e[1;34m $i module doesn't exists !! Cloning now \e[0m"
    			echo -e "\e[1;34m $i module doesn't exists !! Cloning now \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
			clone_module
			echo -e "\e[1;32m $i module cloning is Successfull \e[0m"
    			echo -e "\e[1;32m $i module cloning is Successfull \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
		fi

check_python_env(){    
	if [ "$(which python3.10)" == "/usr/local/bin/python3.10" ] || [ "$(which python3.10)" == "/bin/python3.10" ]; then
		echo -e "\e[1;32m current python env is -> $(which python3.10)\e[0m"
    	else
        	echo -e "\e[1;33m Deactivating python env forcefully\e[0m"
       		deactivate
    	fi
  }
  
create_combine_requirement(){
	echo "creating combined requirement file for module $i"
	cd $WORKING_DIR/$i
	check_python_env
    	source $WORKING_DIR/$i/env/bin/activate
	pip install --upgrade pip >/dev/null 2>&1
	pip install packaging >/dev/null 2>&1
	cp -f ../create-requirements-file.py .
    	python create-requirements-file.py $i
    	deactivate
    	cd $WORKING_DIR
    }

create_python_env(){
	echo "pip module installation started !! This is a time taking process !! Please Wait"

	if [ -d "$WORKING_DIR/$i/env"  ]; then
		rm -rf $WORKING_DIR/$i/env
		echo "Broken ENV deleted"
	fi
    	check_python_env
	cd $WORKING_DIR/$i
	python3.10 -m venv env
    	sleep 3
	cd $WORKING_DIR
    
	if [ ! -f "$WORKING_DIR/$i/requirements_automate.txt" ]; then
       		echo "requirements_automate.txt does not exist in $i " >> $DEPLOYMENT_SCRIPT_LOG
            	create_combine_requirement
	fi
		
	echo -e " requirements_automate.txt Installation Started in $i "
	echo -e "\n requirements_automate.txt Installation Started in $i " >> $DEPLOYMENT_SCRIPT_LOG
    	source $WORKING_DIR/$i/env/bin/activate
	pip install -r $WORKING_DIR/$i/requirements_automate.txt >/dev/null 2>&1
    	pip_install=$?
	
	if [ $pip_install != 0 ]; then
		echo -e "Something failed during requirement.txt installation $i" \n>> $DEPLOYMENT_SCRIPT_LOG
		echo -e "\e[1;31m Something failed during requirement.txt installation in $i \e[0m"
    	else
		echo -e "requirement.txt installation Successfully completed in $i \n" >> $DEPLOYMENT_SCRIPT_LOG
		echo -e "\e[1;32m requirement.txt installation Successfully completed in $i from create_python_env \e[0m"
    	fi
		
	deactivate
	echo "$i python env deactivated"
    	check_pip_dependency
	
}

check_pip_dependency(){
	check_python_env
  	source $WORKING_DIR/$i/env/bin/activate
  	nltk_check=$(pip freeze | grep "nltk==")
		
	if [ ! -z $nltk_check ]; then
		echo "Downloading nltk Modules"
		python -c "import nltk;nltk.download('wordnet');nltk.download('stopwords');nltk.download('punkt');nltk.download('omw-1.4')" >/dev/null 2>&1
		echo "Nltk Download completed"
	else
		echo "No nltk Dependencies"
	fi
		
	spacy_check=$(pip freeze | grep "spacy==")
	
	if [ ! -z $spacy_check ]; then
      		echo "Downloading spacy Modules"
      		python -m spacy download en_core_web_sm >/dev/null 2>&1
		python -m spacy download en_core_web_md >/dev/null 2>&1
		echo "Spacy Download completed"
    	else
      	echo "No spacy Dependencies"
    	fi

	deactivate
  	echo "$i python env deactivated"
}

	if [ "$i" == "parabole-zk-system-manager" ] || [ "$i" == "parabole-pipeline-scripts" ] ; then
		echo -e "\e[1;32m Skipping python env for $i\e[0m"

	elif [ -f "$WORKING_DIR/$i/env/bin/activate"  ]; then
		echo -e "\e[1;32m python env already present in \e[1;34m$i\e[0m !! \e[1;32mChecking new pip module added or updated\e[0m"
    		echo -e "\e[1;32m python env already present in \e[1;34m$i\e[0m !! \e[1;32mChecking new pip module added or updated\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
    		create_combine_requirement
		cd $WORKING_DIR/$i
    		check_python_env
		source $WORKING_DIR/$i/env/bin/activate
		pip freeze > $WORKING_DIR/$i/existing_requirements.txt
		pip install --upgrade pip >/dev/null 2>&1
		pip install packaging >/dev/null 2>&1
		cp -f ../create-requirements-file.py .
		python create-requirements-file.py $i update
    		deactivate
    		cd $WORKING_DIR
		
    		if [ -f "$WORKING_DIR/$i/requirements_update.txt" ]; then
      			echo -e "\e[1;33m New pip module found in existing Env!! Updating now \e[0m"
      			echo -e "\e[1;33m New pip module found in existing Env!! Updating now \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
      			check_python_env
      			source $WORKING_DIR/$i/env/bin/activate
      			python -m pip install -r $WORKING_DIR/$i/requirements_update.txt >/dev/null 2>&1
      			pip_install_update_new=$?
      			deactivate
			if [ $pip_install_update_new != 0 ]; then
				echo -e "\e[1;31m Something failed during requirements_update_new.txt installation in $i \e[0m"
        			echo -e "\e[1;31m Something failed during requirements_update_new.txt installation in $i \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
        			echo -e "\e[1;36m Trying to Re-create the Env in $i \e[0m"
        			echo -e "\e[1;36m Trying to Re-create the Env in $i \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
        			create_python_env
        			echo -e "\e[1;32m Python Env Re-created Successfully in $i \e[0m"
        			echo -e "\e[1;32m Python Env Re-created Successfully in $i \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
			else
        			check_pip_dependency
				echo -e "\e[1;32m New pip module updated successfully in existing Python Env in $i \e[0m"
        			echo -e "\e[1;32m New pip module updated successfully in existing Python Env in $i \e[0m" >> $DEPLOYMENT_SCRIPT_LOG	
			fi
		else
			echo -e "\e[1;32m Python Env Already Up to date in $i !! Nothing to do \e[0m"
      			echo -e "\e[1;32m Python Env Already Up to date in $i !! Nothing to do \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
		fi

	else
		echo -e "\e[1;36m Python env doesn't exists in $i !! Creating now \e[0m"
    		echo -e "\e[1;36m Python env doesn't exists in $i !! Creating now \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
    		create_python_env
		echo -e "\e[1;32m Python Env created Successfully in $i \e[0m"
    		echo -e "\e[1;32m Python Env created Successfully in $i \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
	fi
  	
	rm -f $WORKING_DIR/$i/requirements_update_new.txt $WORKING_DIR/$i/create-requirements-file.py $WORKING_DIR/$i/existing_requirements.txt $WORKING_DIR/$i/requirements_automate.txt $WORKING_DIR/$i/requirements_update.txt
	echo -e "\e[1;32m Succesfully Done with module $i \n\e[0m"
  	echo -e "\e[1;32m Succesfully Done with module $i \n\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
done
}

echo -e "\e[1;33m Installing Pipeline Dependencies\e[0m"

#Installing Pipeline Dependencies
pull_parser_image(){
	cd $WORKING_DIR
	sudo rm -f $WORKING_DIR/my_password.txt
	echo "Parabole@123" >> $WORKING_DIR/my_password.txt
	chmod 777 $WORKING_DIR/my_password.txt
  	cat $WORKING_DIR/my_password.txt | docker login --username parabole --password-stdin
  	sudo rm -f $WORKING_DIR/my_password.txt
  	echo -e "\e[1;33mPlease wait parabole/axarev-parsr image is pulling from docker hub !! It might take some time\n\e[0m"
  	docker image pull parabole/axarev-parsr
}

check_zookeeper_kafka_log_config(){
#zookeeper log check
zookeeper_exiting_path=$(cat $KAFKA_PATH/config/zookeeper.properties | grep dataDir)
zookeeper_log_path=$WORKING_DIR/temp/zookeeper_logs
zookeeper_base_path="dataDir="
zookeper_new_path=$zookeeper_base_path$zookeeper_log_path
if [ $zookeeper_exiting_path == $zookeper_new_path ]; then
  echo -e "\e[1;32m zookeeper config alredy prsent in current deployment path\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
  echo -e "\e[1;32m zookeeper config alredy prsent in current deployment path\e[0m"
else
  echo -e "\e[1;32m updating zookeeper config in current deployment path\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
  echo -e "\e[1;32m updating zookeeper config in current deployment path\e[0m"
  sed -i "s~dataDir=.*~${zookeper_new_path}~g" $KAFKA_PATH/config/zookeeper.properties
  mkdir -p $zookeeper_log_path
fi

#kafka log check
kafka_exiting_path=$(cat $KAFKA_PATH/config/server.properties | grep log.dirs)
kafka_log_path=$WORKING_DIR/temp/kafka_logs
kafka_base_path="log.dirs="
kafka_new_path=$kafka_base_path$kafka_log_path
if [ $kafka_exiting_path == $kafka_new_path ]; then
  echo -e "\e[1;32m kafka config alredy prsent in current deployment path\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
  echo -e "\e[1;32m kafka config alredy prsent in current deployment path\e[0m"
else
  echo -e "\e[1;32m updating kafka config in current deployment path\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
  echo -e "\e[1;32m updating kafka config in current deployment path\e[0m"
  sed -i "s~log.dirs=.*~${kafka_new_path}~g" $KAFKA_PATH/config/server.properties
  mkdir -p $kafka_log_path
fi
}

#installing node
node_install(){
	echo -e"\e[1;34m Installing Node COMMAND\e[0m"
  	sudo curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
	node_download=$?
	
	if [ $node_download != 0 ]; then
    		echo -e "\e[1;31m Failed to Download node14.X\e[0m"
    		echo -e "\e[1;31m Failed to Download node14.X\e[0m" >> $DEPLOYMENT_SCRIPT_LOG
  	fi

	sudo $system install -y nodejs
  	node_version=$?
  	if [ $node_version != 0 ]; then
    		echo -e "\e[1;31m Node Installation Failed in $system \e[0m"
    		echo -e "\e[1;31m Node Installation Failed in $system \e[0m" >> $DEPLOYMENT_SCRIPT_LOG
  	fi
	
	echo -e "\e[1;32m Done with Node Installation \e[0m"
}

check_node_installation(){
  	if [ -x "$(command -v node)" ]; then
 	  	echo -e "\e[1;36m Node Command Already Present!!Checking Node version is 14 or above\e[0m"
    		n_v=$(cut -d "." -f1 <<< "$(node -v)")
	  	
		if [ $n_v == "v14" ] || [ $n_v == "v15" ] || [ $n_v == "v16" ] || [ $n_v == "v17" ]; then
  	  		echo -e "\e[1;32m Node Version is Already Intalled !! Nothing to do\e[0m"
	 	else
		  	echo -e "\e[1;36m Node Command Has older Version !! Trying to Install node14.x \e[0m"
		  	echo "Node Command Has older Version !! Trying to Install node14.x" >> $DEPLOYMENT_SCRIPT_LOG
		  	node_install
    		fi
  	else
	  	echo -e "\e[1;36m Node Command not Found !! Trying to Install \e[0m"
	 	echo "Node Command not Found !! Trying to Install" >> $DEPLOYMENT_SCRIPT_LOG
  		node_install
  	fi
}

#python sub module dependencies
html_parser_with_depndencies(){
  	echo -e "\e[1;34m Addding sub module in structure-extractor \e[0m"
  	cd $WORKING_DIR/parabole-structure-extractor
  	sudo rm -rf parabole-html-parser
  	parser="parabole-html-parser"
  	base_url1="https://$user_name:$user_pwd@$url$parser.git"
  	sudo git clone $base_url1 >/dev/null 2>&1
  	cd parabole-html-parser && npm install >/dev/null 2>&1
  	echo "npm modules added in parabole-html-parser and installed pointer $(pwd)"
  	sudo git clone https://github.com/mozilla/readability.git >/dev/null 2>&1
  	cd readability && npm install >/dev/null 2>&1
  	echo "npm modules added in readability and installing pointer $(pwd)"
  	cd $WORKING_DIR
}

#Adding Bert Models
install_unsupervised_model(){
  	cd $WORKING_DIR
  	unsupervised_model=$WORKING_DIR/unsupervised_model
  
	if [ ! -d "$unsupervised_model" ];then
	  	mkdir $unsupervised_model
  	fi

  	MODEL1=$WORKING_DIR/unsupervised_model/uncased_L-12_H-768_A-12
  	
	if [ -d "$MODEL1" ]; then
     		echo -e "\e[1;32m Uncased file already present in Bert-Sepervised\e[0m"
  	else
	  	cd $WORKING_DIR/unsupervised_model
	  	sudo wget https://storage.googleapis.com/bert_models/2018_10_18/uncased_L-12_H-768_A-12.zip 
  		sudo unzip -o uncased_L-12_H-768_A-12.zip 
	  	sudo rm -rf uncased_L-12_H-768_A-12.zip
	  	cd $WORKING_DIR
	  	echo -e "\e[1;32m Uncased file Downloaded in Bert-Sepervised\e[0m"
  	fi

  	MODEL2=$WORKING_DIR/unsupervised_model/cased_L-12_H-768_A-12
  	if [ -d "$MODEL2" ]; then
	  	echo -e "\e[1;32m Cased file already present in Bert-Sepervised \e[0m"
  	else
	  	cd $WORKING_DIR/unsupervised_model 
  		sudo wget https://storage.googleapis.com/bert_models/2018_10_18/cased_L-12_H-768_A-12.zip
  		sudo unzip -o cased_L-12_H-768_A-12.zip
  		sudo rm -rf cased_L-12_H-768_A-12.zip
	  	cd $WORKING_DIR
	  	echo -e "\e[1;32m Cased file Downloaded in Bert-Sepervised\e[0m"
  	fi
}

#Adding spervised/bert models
#supervised Causal module file
install_supervised_causal_models(){
  	if [ -f "$WORKING_DIR/parabole-supervised-causal-generator/causal_default/pytorch_model.bin" ]; then
	  	echo -e "\e[1;32m supervised Causal module Data already exists...\e[0m"
  	else
	  	echo "Downloading Causal module Data in..."
	  	cd $WORKING_DIR/parabole-supervised-causal-generator
	  	sudo $AzCopy copy 'https://parabolebasepackage.blob.core.windows.net/basepackage/causal_default.zip?sp=rw&st=2022-10-13T08:09:41Z&se=2024-02-01T16:09:41Z&spr=https&sv=2021-06-08&sr=c&sig=gBhKD1WweSsLS2LwFRdZgiPhJbqgTulGFuBQ%2FoTEGKc%3D' .
  		echo "Unzipping Causal module Data..."
  		sudo unzip -o causal_default.zip >/dev/null 2>&1
  		sudo rm -rf causal_default.zip
		cd $WORKING_DIR
  	fi
}

#parabole-supervised-entity-generator model file
install_supervised_entity_models(){
  if [ -f "$WORKING_DIR/parabole-supervised-entity-generator/entity_default/pytorch_model.bin" ]; then
          echo -e "\e[1;32m parabole-supervised-entity-generator module Data already exists...\e[0m"
  else
          echo "Downloading parabole-supervised-entity-generator module Data in..."
          cd $WORKING_DIR/parabole-supervised-entity-generator
          sudo $AzCopy copy 'https://parabolebasepackage.blob.core.windows.net/basepackage/causal_default.zip?sp=rw&st=2022-10-13T08:09:41Z&se=2024-02-01T16:09:41Z&spr=https&sv=2021-06-08&sr=c&sig=gBhKD1WweSsLS2LwFRdZgiPhJbqgTulGFuBQ%2FoTEGKc%3D' .
        echo "Unzipping supervised-entity module Data..."
        sudo unzip -o causal_default.zip >/dev/null 2>&1
        sudo mv causal_default entity_default
        sudo rm -rf causal_default.zip
                cd $WORKING_DIR
  fi
}


#supervised Causal module file module -> "parabole-analysis-proportionality-generator"
install_analysis_models(){
  	proportionality_model_path1=$WORKING_DIR/parabole-analysis-proportionality-generator/models/bart_model/pytorch_model.bin
  	proportionality_model_path2=$WORKING_DIR/parabole-analysis-proportionality-generator/models/default_model/pytorch_model.bin
  	if [ -f "$proportionality_model_path1" ] && [ -f "$proportionality_model_path2" ]; then
    		echo -e "\e[1;32m supervised Causal module Data already exists...\e[0m"
  	else
    		echo "Downloading Causal module Data in..."
    		cd $WORKING_DIR/parabole-analysis-proportionality-generator
    		rm -rf models
    		mkdir models
    		sudo $AzCopy copy 'https://parabolebasepackage.blob.core.windows.net/basepackage/bart-large-mnli.zip?sp=rw&st=2022-10-13T08:09:41Z&se=2024-02-01T16:09:41Z&spr=https&sv=2021-06-08&sr=c&sig=gBhKD1WweSsLS2LwFRdZgiPhJbqgTulGFuBQ%2FoTEGKc%3D' .
    		sudo $AzCopy copy 'https://parabolebasepackage.blob.core.windows.net/basepackage/Cause-Effect_256_FINE_TUNED_NER_bert-large-cased_v2_150422.zip?sp=rw&st=2022-10-13T08:09:41Z&se=2024-02-01T16:09:41Z&spr=https&sv=2021-06-08&sr=c&sig=gBhKD1WweSsLS2LwFRdZgiPhJbqgTulGFuBQ%2FoTEGKc%3D' .
    		echo "Unzipping analysis-proportionality module Data..."
    		sudo unzip -o bart-large-mnli.zip >/dev/null 2>&1
    		sudo unzip -o Cause-Effect_256_FINE_TUNED_NER_bert-large-cased_v2_150422.zip >/dev/null 2>&1
    		sudo rm -rf bart-large-mnli.zip Cause-Effect_256_FINE_TUNED_NER_bert-large-cased_v2_150422.zip
    		sudo mv bart-large-mnli models/bart_model
    		sudo mv Cause-Effect_256_FINE_TUNED_NER_bert-large-cased_v2_150422 models/default_model
    		cd $WORKING_DIR
  	fi
}

#Adding Universal Sentece Encoder module --> parabole sentence-ranker
install_universal_sentence_encoder(){
  	encoder_path=$WORKING_DIR/universal-sentence-encoder
  	if [ ! -d $encoder_path ]; then
	  	sudo mkdir $encoder_path
  	fi

  	if [ -d "$encoder_path/USE_3_large" ] ; then
	  	echo -e "\e[1;32m Universal Sentence Encoder Already Pressent\e[0m"
  	else
   		sudo mv $WORKING_DIR/USE_3_large $encoder_path
  	fi
}

#Creating Pipeline Log Directory
module_terminal_log_folder(){
  	if [ -d "$LOG_DIR" ]; then
 	  	echo -e "\e[1;32m Log Directory already exists!!!\e[0m"
  	else
 	  	echo "Creating Log Directory!!"
   		sudo mkdir $LOG_DIR
   		sudo chmod 777 -R $LOG_DIR
  	fi
}

#deleting script file for conflicted use
delete_files(){
  	rm -f $WORKING_DIR/clone_learning_modules_by_tag.bash
  	cd $WORKING_DIR
  	./delete_zk_system_manager.sh
  	rm -rf delete_zk_system_manager.sh backup_env config
  	echo -e "\e[1;32mParabole Pipeline Installed Successfully !! Thanks for using it !! Rakesh Debnath\e[0m"
}

#Install Docker
install_docker(){
  	if [ -x "$(command -v docker)" ]; then
	  	echo -e "\e[1;32m Docker Already Present\e[0m \n"
    		echo -e "\e[1;32m Docker Already Present\e[0m \n" >> $DEPLOYMENT_SCRIPT_LOG
  	elif [ $system == "apt-get" ]; then
    		echo -e "\e[1;34m Installing Docker in Ubuntu\e[0m \n"
    		echo -e "\e[1;34m Installing Docker in Ubuntu\e[0m \n" >> $DEPLOYMENT_SCRIPT_LOG
    		sudo apt-get update > /dev/null 2>&1
    		sudo apt-get install \
    		ca-certificates \
    		curl \
    		gnupg \
    		lsb-release > /dev/null 2>&1
    		sudo mkdir -p /etc/apt/keyrings
    		curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg > /dev/null 2>&1
    		echo \
  		"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  		$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    		sudo apt-get update -y > /dev/null 2>&1
    		docker_gpg_update=$?
		
		if [ $docker_gpg_update != 0 ]; then   
      			echo "Docker gpg Formatting"
      			sudo chmod a+r /etc/apt/keyrings/docker.gpg
      			sudo apt-get update -y > /dev/null 2>&1
    		fi
    	
		sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y > /dev/null 2>&1
    	
		if [ -x "$(command -v docker)" ]; then
      			echo -e "\e[1;32m Successfully installed Docker\e[0m \n"
      			echo -e "\e[1;32m Successfully installed Docker\e[0m \n" >> $DEPLOYMENT_SCRIPT_LOG
    		else
      			echo -e "\e[1;32m Failed to install Docker\e[0m \n"
      			echo -e "\e[1;32m Failed to install Docker\e[0m \n" >> $DEPLOYMENT_SCRIPT_LOG
    		fi
  	else
    		echo "No automated Docker Installation for RHEL Release"
  	fi
}  

if [ $# -eq 0 ] || [ "$1" == "deploy" ] || [ "$1" == "-deploy" ] || [ "$1" == "--deploy" ]; then
  	os_system_check
  	python_dependent_library
  	activate_cargo_env
  	pull_latest_scripts
  	install_azcopy
  	install_base_package
  	install_jre
  	create_backup_env_folder
  	terminate_pipeline
  	clone_module_create_env
  	install_docker
  	pull_parser_image
    check_zookeeper_kafka_log_config
  	check_node_installation
  	html_parser_with_depndencies
  	install_unsupervised_model
  	install_supervised_causal_models
    install_supervised_entity_models
  	install_analysis_models
  	install_universal_sentence_encoder
  	module_terminal_log_folder
  	delete_files
fi

if [ "$1" == "parsr" ] || [ "$1" == "-parsr" ] || [ "$1" == "--parsr" ]; then
	install_docker
  	pull_parser_image
fi

if [ "$1" == "scripts" ] || [ "$1" == "-scripts" ] || [ "$1" == "--scripts" ]; then
	user_input_for_branch_selection
    	pull_latest_scripts
fi

if [ "$1" == "password" ] || [ "$1" == "-password" ] || [ "$1" == "--password" ]; then
	echo $user_pwd
fi

#Script Description
if [ "$1" == "help" ] || [ "$1" == "-help" ] || [ "$1" == "--help" ]; then
	printf "command : "
	echo "./clone_learning_modules_by_tag.bash"
	printf "script_details : "
	echo "run this script to update or deploy latest pipeline with all the dependencies installled"
	echo
    	printf "command : "
	echo "./clone_learning_modules_by_tag.bash --password"
	printf "script_details : "
	echo "run this script to get bitbucker app password for repo.manager@parabole.ai"
	echo
fi
