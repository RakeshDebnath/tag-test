#pipeline working dir

#TRAIN_DEPLOYMENT_PATH=/home/ubuntu/train/standard_script/test
if [[ -z "$TRAIN_DEPLOYMENT_PATH" ]]; then
        SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"
        WORKING_DIR=$SCRIPT_PATH
else
        cd $TRAIN_DEPLOYMENT_PATH
        WORKING_DIR=$(pwd)
	#sed -i "1iTRAIN_DEPLOYMENT_PATH=$TRAIN_DEPLOYMENT_PATH" parabole_base_dependency_installation.bash
fi
echo "before sed $(pwd)"
sed -i "1iWORKING_DIR=$WORKING_DIR" parabole_base_dependency_installation.bash

#printing pipeline directory information
echo -ne "\e[1;33mPipeline Directory selection : \n\e[0m  \e[1;32mWORKING_DIR=$WORKING_DIR\e[0m" 
if [[ -n "$SCRIPT_PATH" ]]; then
	echo -e "\e[1;32m && SCRIPT_PATH=$SCRIPT_PATH\e[0m" 
fi
if [[ -n "$TRAIN_DEPLOYMENT_PATH" ]]; then
	echo -e "\e[1;32m && TRAIN_DEPLOYMENT_PATH=$TRAIN_DEPLOYMENT_PATH\e[0m"
fi


echo "before source $(pwd)"
. ./parabole_base_dependency_installation.bash
echo "script trigger"
install_azcopy

