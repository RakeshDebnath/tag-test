WORKING_DIR=$(pwd)
echo $WORKING_DIR
echo $MODULE_BRANCH
source $WORKING_DIR/module_list_config.sh
for module_name in "${module_list[@]}"
              do
                        echo "adding module $module_name to excel"
                        echo $module_name >> modules.csv
        done

unix2dos modules.csv
ssconvert modules.csv parabole-airo-runtime-modules.xlsx


              for module_name in "${module_list[@]}"
              do
  		        echo "Working with module $module_name"
                if [ $1 == "true" ]; then
                  echo "Deleting older for conflicts"
                  rm -rf $module_name
                fi
                echo "https://${REPO_MANAGER_USER_NAME}:${REPO_MANAGER_APP_PASSWORD}@bitbucket.org/${REPO_MANAGER_WORKSPACE}/${module_name}.git"
                git clone https://${REPO_MANAGER_USER_NAME}:${REPO_MANAGER_APP_PASSWORD}@bitbucket.org/${REPO_MANAGER_WORKSPACE}/${module_name}.git
                  cd $module_name
                  git checkout $MODULE_BRANCH
              if [ "$ENCRYPTION" == "true" ]; then
                cp ../python_encryption_single_module.py ./;
				python3.10 -m pip install --upgrade pip
                python3.10 -m pip install Cython;
                python3.10 python_encryption_single_module.py build_ext --inplace;
               else
               echo "Module encryption is skipped !! ENCRYPTION -> $ENCRYPTION";
              fi
              cd $WORKING_DIR
              done
rm -rf clone_module.bash python_encryption_single_module.py module_list_config.sh modules.csv
ls -al