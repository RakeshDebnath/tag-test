#!/bin/bash

# Termination script for learning pipeline
if [[ -z "$2" ]]; then
   	WORKING_DIR=$(pwd)
else
	cd $2
	WORKING_DIR=$(pwd)
fi
echo $WORKING_DIR
ZK_PATH=$WORKING_DIR/kafka_2.11-2.3.0
KAFKA_PATH=$WORKING_DIR/kafka_2.11-2.3.0
SOLR_PATH=$WORKING_DIR/solr-8.3.0
hostname=$(hostname -I | awk '{print $1}')

echo -e "\nParabole pipeline Terminate script executed"
if [ "$1" == "all" ] || [ "$1" == "-all" ] || [ "$1" == "--all" ] && [ $hostname != "172.16.214.22" ]; then

	sudo rm -Rf /tmp/kafka-logs $WORKING_DIR/clone_learning_modules_by_tag.bash $WORKING_DIR/tag_learning_modules.sh
	echo -e "\nKafka-log folder removed from /tmp/kafka-logs/"
	
	#Stopping docker container for hexa-parser
	if [ $( sudo docker ps -a | grep axarev-parsr | wc -l ) -gt 0 ]; then
		echo "axarev-parser stopped"
		sudo docker stop axarev-parsr
		sudo docker rm -f axarev-parsr
	fi
    echo "stopping kafka service"
	cd $KAFKA_PATH/bin
	sudo sh kafka-server-stop.sh
	cd $WORKING_DIR
	sleep 20

	tika=$(ps -ef | grep tika-server.jar | grep -v grep | awk '{print $2}')
	sudo kill -9 $tika >/dev/null 2>&1

	cd $KAFKA_PATH/bin
	echo $pwd
	sudo sh zookeeper-server-stop.sh
	cd $WORKING_DIR
	sleep 5

	sudo $SOLR_PATH/bin/solr stop -c -p 8983
	echo "Killed Solr"
	
	function kafka_process_check {
        ps -ef | grep -i 'kafka\.Kafka' | grep -v grep | awk '{print $2}'
        }
kafka_process=$(kafka_process_check)
echo "Checking Kafka process"
while [ ! -z $kafka_process ]
do
        time=5
        sleep $time
        time_count=$(( $time_count + $time ))
        echo "Waiting for Kafka process to Stop !! Max wait time 30sec !! current time ${time_count}sec"
        if [ $time_count == 30 ]; then
                echo -e "\e[1;31m\nUnable to Stop kafka using termination script !! Please force kill it or reach out to System Admin\e[0m"
                                echo -e "\e[1;34m\nyou can execute bellow command to force kill it\e[0m"
                                echo "kafka_process=\$(ps -ef | grep -i 'kafka\.Kafka' | grep -v grep | awk '{print \$2}'); kill -9 \$kafka_process"
                                echo
                                exit
        fi
        kafka_process=$(kafka_process_check)
done
elif [ $hostname == "172.16.214.22" ]; then
        echo -e "\e[0;32mBase package Termination Skipped for server 22 as it is running as central service \e[0m"

else
        echo  

fi


echo "Terminating parabole pipeline"
sudo rm -rf $WORKING_DIR/logs/server_time_log

learning_starter=$(ps -ef | grep gunicorn | grep -v grep | awk '{print $2}')
sudo kill -9 $learning_starter >/dev/null 2>&1

learning_starter_metadata=$(ps -ef | grep metadata_driver.py | grep -v grep | awk '{print $2}')
sudo kill -9 $learning_starter_metadata >/dev/null 2>&1

ontology=$(ps -ef | grep auto_ontology_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $ontology >/dev/null 2>&1

analysis_model=$(ps -ef | grep analysis_component_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $analysis_model >/dev/null 2>&1

bert_validator=$(ps -ef | grep bert_validator_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $bert_validator >/dev/null 2>&1

conll_generator=$(ps -ef | grep core_nlp_generator | grep -v grep | awk '{print $2}')
sudo kill -9 $conll_generator >/dev/null 2>&1

corpus_downloader=$(ps -ef | grep corpus_downloader_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $corpus_downloader >/dev/null 2>&1

document_reader=$(ps -ef | grep document_reader_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $document_reader >/dev/null 2>&1

document_filter=$(ps -ef | grep document_filter_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $document_filter >/dev/null 2>&1

graph_creator=$(ps -ef | grep graph_creator_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $graph_creator >/dev/null 2>&1

flow_controller=$(ps -ef | grep controller_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $flow_controller >/dev/null 2>&1

sentence_ranker=$(ps -ef | grep sentence_ranker | grep -v grep | awk '{print $2}')
sudo kill -9 $sentence_ranker >/dev/null 2>&1

sentence_ranker1=$(ps -ef | grep bert_second_level_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $sentence_ranker1 >/dev/null 2>&1

structure_extractor=$(ps -ef | grep extractor_driver.py | grep -v grep | awk '{print $2}')
sudo kill -9 $structure_extractor >/dev/null 2>&1

wordspace_generator=$(ps -ef | grep wordspace_generator_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $wordspace_generator >/dev/null 2>&1

wordspace_validation=$(ps -ef | grep wordspace_validation_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $wordspace_validation >/dev/null 2>&1

user_review=$(ps -ef | grep review_golden | grep -v grep | awk '{print $2}')
sudo kill -9 $user_review >/dev/null 2>&1

analysis_model=$(ps -ef | grep analysis_component_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $analysis_model >/dev/null 2>&1

learning_starter_metadata_old=$(ps -ef | grep metadata.py | grep -v grep | awk '{print $2}')
sudo kill -9 $learning_starter_metadata_old >/dev/null 2>&1

languagetool=$(ps -ef | grep languagetool.server.HTTPServer | grep -v grep | awk '{print $2}')
sudo kill -9 $languagetool >/dev/null 2>&1

joblib=$(ps -ef | grep loky.backend.popen_loky_posix | grep -v grep | awk '{print $2}')
sudo kill -9 $joblib >/dev/null 2>&1

supervised_generator=$(ps -ef | grep supervised_generator_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $supervised_generator >/dev/null 2>&1

supervised_generator_causal=$(ps -ef | grep supervised_causal_generator_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $supervised_generator_causal >/dev/null 2>&1

analysis_proportionality_generator=$(ps -ef | grep analysis_proportionality_generator_driver | grep -v grep | awk '{print $2}')
sudo kill -9 $analysis_proportionality_generator >/dev/null 2>&1

multiprocessing=$(ps -ef | grep "multiprocessing." | grep -v grep | awk '{print $2}')
sudo kill -9 $multiprocessing >/dev/null 2>&1

output_validator=$(ps -ef | grep "graph_validation_driver" | grep -v grep | awk '{print $2}')
sudo kill -9 $output_validator >/dev/null 2>&1

supervised_entity_generator=$(ps -ef | grep "supervised_entity_generator_driver" | grep -v grep | awk '{print $2}')
sudo kill -9 $supervised_entity_generator >/dev/null 2>&1

analysis_entity_generator=$(ps -ef | grep "analysis_entity_generator_driver.py" | grep -v grep | awk '{print $2}')
sudo kill -9 $analysis_entity_generator >/dev/null 2>&1

echo "Parabole pipeline Terminate script ended"
