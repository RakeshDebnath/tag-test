import os
import sys
import json
import storage_config as cfg
from storage_utils import Storage
import hashlib


class Naming:
    file_dict = {}

    @staticmethod
    def write():
        path = os.path.join(Storage.get_storage_prefix(), cfg.map_file_names)
        Storage.write(path, bytes(json.dumps(Naming.file_dict, default=str), 'utf-8'), is_absolute=True)

    @staticmethod
    def file_hash_name(file_contents, filename=''):
        new_name = hashlib.md5(file_contents).hexdigest() + os.path.splitext(filename)[-1]

        if filename:
            Naming.file_dict[filename] = new_name
        return new_name

    @staticmethod
    def get_original_filename(hash_name):
        return Naming.file_dict[hash_name]

    @staticmethod
    def initialize():
        try:
            path = os.path.join(Storage.get_storage_prefix(), cfg.map_file_names)
            if Storage.file_exists(path, is_absolute=True):
                Naming.file_dict = json.loads(Storage.read(path, is_absolute=True))
            else:
                Naming.file_dict = {}
        except:
            pass


if __name__ == "__main__":
    print(Naming().file_hash_name(open(str(sys.argv[1]), 'rb').read()))
