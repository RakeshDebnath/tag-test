import time
from kafka_utils import CallBack, KafkaService
from multiprocessing import Process
from threading import current_thread

class Response(CallBack):
    def success(self, key, value=None):
        print(f'In {current_thread().getName()} success implementation. key: {key}; value: {value}')

    def error(self, key, error_message):
        print(f'In {current_thread().getName()} error implementation. key: {key}; error_message: {error_message}')

class TestProducer(Process):
    kafka = KafkaService()
    topic = 'topic_p4_5'

    def run(self):
        with open('test/sample.txt', 'r', encoding='utf-8') as file:
            content = file.read()

        self.kafka.send(topic=self.topic,
                        key=b'first message',
                        payload={'content': content},
                        callback=Response())
        '''time.sleep(1)
        self.kafka.send(topic=self.topic,
                        key=b'Second message',
                        payload={'language': 'Python'},
                        callback=Response())
        time.sleep(1)
        self.kafka.send(topic=self.topic,
                        key=b'Third message',
                        payload={'project': 'Corpus generation'},
                        callback=Response())'''

if __name__=='__main__':
    TestProducer().start()

    while True:
        print(f'{current_thread().getName()} is runnig. . .')
        time.sleep(30)