#!/bin/bash

#Pipeline start script
if [[ -z "$2" ]]; then
        WORKING_DIR=$(pwd)
else
        cd $2
        WORKING_DIR=$(pwd)
fi
echo $WORKING_DIR
ZK_PATH=$WORKING_DIR/kafka_2.11-2.3.0
KAFKA_PATH=$WORKING_DIR/kafka_2.11-2.3.0
SOLR_PATH=$WORKING_DIR/solr-8.3.0

#ZOOKEEPER_SERVER="${1:-localhost:2181}"

hostname=$(hostname -I | awk '{print $1}')
export PYTHONPATH=$WORKING_DIR/parabole-zk-system-manager
sudo cp -f $WORKING_DIR/train_env.txt $WORKING_DIR/train_env_temp.txt
sudo sed -i '/#/d' $WORKING_DIR/train_env_temp.txt
sudo awk 'NF' $WORKING_DIR/train_env_temp.txt >/dev/null 2>&1
cred_arr=$(cat $WORKING_DIR/train_env_temp.txt)
#aading in shell env
for i in "${cred_arr[@]}"
do
        export $i
done
sudo rm -f $WORKING_DIR/train_env_temp.txt

if [ "$1" == "all" ] || [ "$1" == "-all" ] || [ "$1" == "--all" ] && [ $hostname != "172.16.214.22" ]; then
        kafka_pid=$(ps -ef | grep 'kafka\.Kafka' | grep -i 'zookeeper.properties' | grep -v grep | awk '{print $2}')
        echo $kafka_pid
        if [ -n "$kafka_pid" ]; then
                echo "Kafka Service is already running in the Background !! Terminating Start script!!!!"
                echo "Please trigger ./pipeline_terminate.bash --all "
                exit
        fi
        
        if [ -x "$(command -v nvidia-smi)" ] ; then
                sudo sh -c 'docker run -d --gpus all --name axarev-parsr -p 3001:3001 parabole/axarev-parsr' &> $WORKING_DIR/logs/axarev-parsr.log
        else
                sudo sh -c 'docker run -d --name axarev-parsr -p 3001:3001 parabole/axarev-parsr' &> $WORKING_DIR/logs/axarev-parsr.log
        fi
        
#Starting Dependent Services
        echo "Create the kafka-log dir"
        if [ -d $KAFKA_PATH/kafka-logs ]; then
                echo "kafka log already exists"
        else
                sudo mkdir $KAFKA_PATH/kafka-logs
        fi
        : '
        $SOLR_PATH/bin/solr start -c -p 8983 -m 4g -Dsolr.ltr.enabled=true -force
        sleep 5
        echo
        '
        sudo sh -c "java -jar $WORKING_DIR/tika-server.jar" &>  $WORKING_DIR/logs/tika_terminal_log &
	sleep 5
	sudo chmod 777 /tmp/tika.log
        sudo sh -c "$ZK_PATH/bin/zookeeper-server-start.sh $ZK_PATH/config/zookeeper.properties" &> $WORKING_DIR/logs/zookeeper_terminal_output.log &
        sleep 10
        sudo sh -c "$KAFKA_PATH/bin/kafka-server-start.sh $KAFKA_PATH/config/server.properties" &> $WORKING_DIR/logs/kafka_terminal_output.log &
        sleep 5
	kafka_process=$(ps -ef | grep 'kafka\.Kafka' | grep -v grep | awk '{print $2}')
	if [ -n "$kafka_process" ]; then echo -e "\e[1;32mKafka and Zookeper server started Successfully\e[0m" ; else echo -e "\e[1;31mFailed to start Kafka/Zookeeper server\e[0m" ; fi

elif [ $hostname == "172.16.214.22" ]; then
	echo -e "\e[0;32mBase package start Skipped for server 22 as it is running as central service \e[0m"

else
	echo
fi


#Starting Python modules
kafka_pid=$(ps -ef | grep kafka | grep -v grep | awk '{print $2}')
if [ -z "$kafka_pid" ]; then
	echo "Kafka Service is not Running !! Terminating Start script!!!!"
	echo "Please trigger ./pipeline_terminate.bash --all and then ./pipeline_start.bash --all"
	exit
fi

function get_broker_details {
broker_ids_out=$(sudo $WORKING_DIR/zookeeper-3.4.10/bin/zkCli.sh  <<EOF
ls /brokers/ids
quit
EOF
)
broker_ids_csv="$(echo "${broker_ids_out}" | grep '^\[.*\]$')"
echo "$broker_ids_csv" | sed 's/\[//;s/]//;s/,/ /'
}
function zk_process {
        ps -ef | grep zookeeper | grep -v grep | awk '{print $2}'

}
zk_pid=$(zk_process)
while [ -z "$zk_pid" ]; do echo "Zookeper is getting initialized "; sleep 4; zk_pid=$(zk_process); done
echo -e "\e[1;32mZookeper initialized Successfully\e[0m"
sleep 5
return_value=$(get_broker_details)
while [ -z $return_value ]
do
	time=10
        sleep $time
        time_count=$(( $time_count + $time ))
        echo "Please Wait Kafka Broker is getting initialized!!!!"
        sleep $time
	if [ $time_count == 90 ]; then
                echo -e "\e[1;31m\nFailed to initialize Kafka Broker !! Please reach out to System Admin\e[0m"
                exit
        fi
        return_value=$(get_broker_details)

done
echo -e "\e[1;32mKafka Broker initialized Successfully!!\e[0m"


cd $WORKING_DIR/parabole-learning-starter
source $WORKING_DIR/parabole-learning-starter/env/bin/activate
python zk_init.py
zk_init_status=$?
if [ $zk_init_status != 0 ]
then
        echo "Zk Node initilization failed !! Terminating Start script!!!!"
        exit
fi
echo -e "\e[1;32m\nzk node initialized\e[0m"

cd $WORKING_DIR/parabole-learning-starter
gunicorn -c gunicorn_init.py 'learning_starter_driver:main()' &> $WORKING_DIR/logs/gunicorn_terminal_output.log &
#echo "The Learning starter init and drivers started"
python metadata_driver.py &> $WORKING_DIR/logs/starter_metadata.log &
deactivate
#echo "Learning Starter metadata started"

cd $WORKING_DIR/parabole-auto-ontology
source $WORKING_DIR/parabole-auto-ontology/env/bin/activate
python auto_ontology_driver.py -c test/config.ini &> $WORKING_DIR/logs/ontology_terminal.log &
deactivate
#echo "Ontology started"

cd $WORKING_DIR/parabole-analysis-component-generator
source $WORKING_DIR/parabole-analysis-component-generator/env/bin/activate
python analysis_component_driver.py &> $WORKING_DIR/logs/analysis_model_generator.log &
deactivate


cd $WORKING_DIR/parabole-analysis-entity-generator
source $WORKING_DIR/parabole-analysis-entity-generator/env/bin/activate
python analysis_entity_generator_driver.py &> $WORKING_DIR/logs/analysis_entity_generator.log &
deactivate

cd $WORKING_DIR/parabole-supervised-generator
source $WORKING_DIR/parabole-supervised-generator/env/bin/activate
python supervised_generator_driver.py &> $WORKING_DIR/logs/supervised_generator_classification.log &
deactivate


cd $WORKING_DIR/parabole-supervised-causal-generator
source $WORKING_DIR/parabole-supervised-causal-generator/env/bin/activate
python supervised_causal_generator_driver.py &> $WORKING_DIR/logs/supervised_generator_causal.log &
deactivate

cd $WORKING_DIR/parabole-supervised-entity-generator
source $WORKING_DIR/parabole-supervised-entity-generator/env/bin/activate
python supervised_entity_generator_driver.py &> $WORKING_DIR/logs/supervised_entity_generator.log &
deactivate

cd $WORKING_DIR/parabole-analysis-proportionality-generator
source $WORKING_DIR/parabole-analysis-proportionality-generator/env/bin/activate
python analysis_proportionality_generator_driver.py &> $WORKING_DIR/logs/analysis_proportionality_generator.log &
deactivate

cd $WORKING_DIR/parabole-bert-validator
source $WORKING_DIR/parabole-bert-validator/env/bin/activate
python bert_validator_driver.py &> $WORKING_DIR/logs/bert_validator.log &
deactivate
#echo "bert validator started"

cd $WORKING_DIR/parabole-conll-generator-spacy
source $WORKING_DIR/parabole-conll-generator-spacy/env/bin/activate
python core_nlp_generator_driver.py &> $WORKING_DIR/logs/conll_generator_terminal.log &
deactivate
#echo "conll_generator started"

cd $WORKING_DIR/parabole-corpus-downloader
source $WORKING_DIR/parabole-corpus-downloader/env/bin/activate
python corpus_downloader_driver.py &> $WORKING_DIR/logs/document_downloader_terminal.log &
deactivate
#echo "Downloader started"

cd $WORKING_DIR/parabole-document-filter
source $WORKING_DIR/parabole-document-filter/env/bin/activate
python document_filter_driver.py &> $WORKING_DIR/logs/document_filter_terminal.log &
deactivate
#echo "Document-filter started"

cd $WORKING_DIR/parabole-document-reader
source $WORKING_DIR/parabole-document-reader/env/bin/activate
python document_reader_driver.py &> $WORKING_DIR/logs/document_reader_terminal.log &
deactivate
#echo "Document-reader module started"

cd $WORKING_DIR/parabole-flow-controller
source $WORKING_DIR/parabole-flow-controller/env/bin/activate
python controller_driver.py &> $WORKING_DIR/logs/controller.log &
deactivate
#echo "Term-filter module started"

cd $WORKING_DIR/parabole-graph-creator
source $WORKING_DIR/parabole-graph-creator/env/bin/activate
python graph_creator_driver.py &> $WORKING_DIR/logs/graph_creator.log &
deactivate

cd $WORKING_DIR/parabole-output-validator
source $WORKING_DIR/parabole-output-validator/env/bin/activate
python graph_validation_driver.py &> $WORKING_DIR/logs/output-validator.log &
deactivate

cd $WORKING_DIR/parabole-sentence-ranking
source $WORKING_DIR/parabole-sentence-ranking/env/bin/activate
python sentence_ranker_driver.py &> $WORKING_DIR/logs/sentence_ranker_terminal.log &
#echo "sentence_ranker started"
python bert_second_level_driver.py &> $WORKING_DIR/logs/sentence_ranker_bert_terminal.log &
deactivate
#echo "bert_second_level started"

cd $WORKING_DIR/parabole-structure-extractor
source $WORKING_DIR/parabole-structure-extractor/env/bin/activate
python extractor_driver.py &> $WORKING_DIR/logs/structure_extractor_terminal.log &
deactivate
#echo "parabole structure extractor started"

cd $WORKING_DIR/parabole-user-review
source $WORKING_DIR/parabole-user-review/env/bin/activate
python review_golden_doc_driver.py &> $WORKING_DIR/logs/review_doc.log &
#echo "user review golden doc started"
python review_golden_terms_driver.py &> $WORKING_DIR/logs/review_terms.log &
deactivate
#echo "parabole user review golden term started"

cd $WORKING_DIR/parabole-wordspace-generator
source $WORKING_DIR/parabole-wordspace-generator/env/bin/activate
python wordspace_generator_driver.py &> $WORKING_DIR/logs/wordspace.log &
deactivate
#echo "wordspace started"

cd $WORKING_DIR/parabole-wordspace-validation
source $WORKING_DIR/parabole-wordspace-validation/env/bin/activate
python wordspace_validation_driver.py &> $WORKING_DIR/logs/wordspace_validation.log &
deactivate
#echo "wordspace validation started"

echo -e "\e[1;33mChecking module Validation!!!Please wait\n\e[0m"
sleep 7

if [ $( sudo docker ps -a | grep axarev-parsr | wc -l ) -gt 0 ]; then echo -e "\e[1;32mparabole/axarev-parsr image started succesfully\e[0m"; else echo -e "\e[1;31mFailed to start parabole/axarev-parsr\e[0m" ; fi
: '
solr_process=$(ps ax | grep java | grep -i Dsolr | grep -v grep | awk '{print $1}')
if [ ! -z $solr_process ]; then echo ; echo -e "\e[1;32mSolr started Successfully\e[0m" ; else echo -e "\e[1;31mFailed to start solr\e[0m" ; fi
'
tika_process=$(ps ax | grep java | grep -i tika | grep -v grep | awk '{print $1}')
if [[ ! -z $tika_process ]]; then echo -e "\e[1;32mTika server started Successfully\e[0m" ; else echo -e "\e[1;31mFailed to start Tika server\e[0m" ; fi

kafka_crash=$(ps -ef | grep 'kafka\.Kafka' | grep -v grep | awk '{print $2}')
	if [ -z "$kafka_crash" ]; then echo -e "\e[1;31mKafka or Zookeper server got Crashed\e[0m" ; else  echo -e "\e[1;32mKafka server started Successfully\e[0m"; fi
#Process check
declare -A arr=( [parabole-learning-starter]="gunicorn_init.py" [parabole-learning-starter~]="metadata_driver.py" [parabole-auto-ontology]="auto_ontology_driver.py" [parabole-analysis-component-generator]="analysis_component_driver.py" [parabole-analysis-proportionality-generator]="analysis_proportionality_generator_driver" [parabole-analysis-entity-generator]="analysis_entity_generator_driver" [parabole-supervised-generator]="supervised_generator_driver.py" [parabole-supervised-causal-generator]="supervised_causal_generator_driver.py" [parabole-supervised-entity-generator]="supervised_entity_generator_driver.py" [parabole-bert-validator]="supervised_generator_driver.py" [parabole-conll-generator-spacy]="core_nlp_generator_driver.py" [parabole-corpus-downloader]="corpus_downloader_driver.py" [parabole-document-filter]="document_filter_driver.py" [parabole-document-reader]="document_reader_driver.py" [parabole-flow-controller]="controller_driver.py" [parabole-graph-creator]="graph_creator_driver.py" [parabole-output-validator]="graph_validation_driver.py" [parabole-sentence-ranking]="sentence_ranker_driver.py" [parabole-sentence-ranking~]="bert_second_level_driver.py" [parabole-structure-extractor]="extractor_driver.py" [parabole-user-review]="review_golden_doc_driver.py" [parabole-user-review~]="review_golden_terms_driver.py" [parabole-wordspace-generator]="wordspace_generator_driver.py" [parabole-wordspace-validation]="wordspace_validation_driver.py")

for key in ${!arr[@]}; do
	process_id=$(ps -ef | grep  ${arr[${key}]} | grep -v grep | awk '{print $2}')
    	if [ -z "$process_id" ]; then 
		echo -e "\e[1;31mFailed to start file ${arr[${key}]} in module named ${key} !! No Process Found \n\e[0m" 
	else
		echo -e "\e[1;32m${key} module started successfully\n\e[0m"
	fi
	sleep 1
done

echo "Parabole pipeline script ended"
