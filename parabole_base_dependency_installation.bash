#Installing AZCOPY
install_azcopy(){
	AzCopy=$WORKING_DIR/azcopy
	echo $AzCopy
	if test -f "$AzCopy"; then
		echo -e "\e[1;32m AzCopy already exists.\e[0m"
  	else
		cd $WORKING_DIR
		echo "Installing AzCopy in...$(pwd)"
  		wget -O azcopy_v10.tar.gz https://aka.ms/downloadazcopy-v10-linux >/dev/null 2>&1
  		tar -tf azcopy_v10.tar.gz >/dev/null 2>&1
  		tar -xf azcopy_v10.tar.gz --strip-components 1
  		rm -rf azcopy_v10.tar.gz  ThirdPartyNotice.txt NOTICE.txt
  	fi
}




