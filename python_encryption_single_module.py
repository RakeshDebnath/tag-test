from setuptools import setup
from setuptools.extension import Extension

from Cython.Build import cythonize
from Cython.Distutils import build_ext
import os
import shutil

os.remove("python_encryption_single_module.py")
name1 = os.path.basename(os.getcwd())
extension_list = []
to_be_deleted = []
for file1 in os.listdir():
    exclude_files = ["config", "driver", "Driver", "cfg", "_init"]
    if (file1[-3:] == ".py"):
        print([file1[:-3], [file1]])
        if any(x in file1 for x in exclude_files):
            print(os.getcwd())
            print("Driver or Config file excluding for Encryption ", file1)
        else:
            try:
                setup(
                    name=name1,
                    ext_modules=cythonize(
                    Extension(file1[:-3], [file1]),
                    build_dir="build",
                    compiler_directives={'language_level': "3", 'always_allow_keywords': True}),
                    cmdclass=dict(build_ext=build_ext),
                    packages=[]
                    )
                to_be_deleted.append(file1)
            except Exception as e:
                print(e)
                print("Cannot cythonise {}".format(file1))

for file2 in to_be_deleted:
    print("Deleting file --> {}".format(file2))
    os.remove(file2)
print("Deleting Build floder")
shutil.rmtree("build")
