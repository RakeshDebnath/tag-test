import os
import json
import storage_config as cfg
from enumerations import ModuleEnum, AttributeEnum, GoldenLinkType, ModelEnum
from zookeeper_utils import Zookeeper
import zk_config
from storage_azure import Azure
from storage_aws import AWS
from storage_local import Local
from storage_network import Network
import logging

logger = logging.getLogger(__name__)
model_mapping_file = 'model_mapping.json'
golden_documents = 'golden_document_list.json'
supervised_model_mapping_file = 'supervised_model_mapping.json'
reviewed_golden_terms = 'input_terms.gol'


class Storage:
    @staticmethod
    def get_storage(storage=None, use_disk=False):
        storage = storage if storage else cfg.current_storage
        if cfg.StorageEnum.LOCAL in storage:
            if use_disk:
                if cfg.StorageEnum.AZURE in storage:
                    return cfg.StorageEnum.AZURE
                if cfg.StorageEnum.AWS in storage:
                    return cfg.StorageEnum.AWS
                if cfg.StorageEnum.GCP in storage:
                    return cfg.StorageEnum.GCP
                if cfg.StorageEnum.NETWORK in storage:
                    return cfg.StorageEnum.NETWORK
            return cfg.StorageEnum.LOCAL
        return storage

    @staticmethod
    def get_base_prefix(storage=None, use_disk=False):
        store = Storage.get_storage(storage, use_disk).name.lower()
        return os.path.join(cfg.storage_cfg[store]["folder"], zk_config.zk_deployment_path)

    @staticmethod
    def get_storage_prefix(storage=None, use_disk=False):
        store = Storage.get_storage(storage, use_disk).name.lower()
        return os.path.join(cfg.storage_cfg[store]["folder"], Zookeeper.get_session_prefix())

    @staticmethod
    def __normalize_path(url, is_absolute, storage, config, from_base=False, iteration=None):
        if is_absolute:
            return url.replace('\\', '/')
        iter_ = '' if from_base else f'iteration_{Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.LEARNING_ITERATION)}'
        iter_ = iter_ if iteration is None else iteration
        return os.path.normpath(os.path.join(Storage.get_storage_prefix(storage), iter_, url)).replace('\\', '/')

    @staticmethod
    def write(file_url, file_data, is_absolute=False, config=None, storage=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        file_url = Storage.__normalize_path(file_url, is_absolute, storage, config, from_base=from_base)

        if storage == cfg.StorageEnum.LOCAL:
            Local.write(file_url, file_data)
        elif storage == cfg.StorageEnum.AWS:
            AWS.write(file_url, file_data, config)
        elif storage == cfg.StorageEnum.AZURE:
            Azure.write(file_url, file_data, config)
        elif storage == cfg.StorageEnum.NETWORK:
            return Network.write(file_url, file_data, config)

    @staticmethod
    def read(file_url, is_absolute=False, config=None, storage=None, iteration=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        file_url = Storage.__normalize_path(file_url, is_absolute, storage, config, iteration=iteration, from_base=from_base)
        if storage == cfg.StorageEnum.LOCAL:
            return Local.read(file_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.read(file_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.read(file_url, config)
        elif storage == cfg.StorageEnum.NETWORK:
            return Network.read(file_url, config)

    @staticmethod
    def file_exists(file_url, is_absolute=False, config=None, storage=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        file_url = Storage.__normalize_path(file_url, is_absolute, storage, config, from_base=from_base)

        if storage == cfg.StorageEnum.LOCAL:
            return Local.file_exists(file_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.file_exists(file_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.file_exists(file_url, config)

    @staticmethod
    def move(src_url, dst_url, is_absolute=False, config=None, storage=None, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        src_url = Storage.__normalize_path(src_url, is_absolute, storage, config)
        dst_url = Storage.__normalize_path(dst_url, is_absolute, storage, config)

        if storage == cfg.StorageEnum.LOCAL:
            Local.move(src_url, dst_url)
        elif storage == cfg.StorageEnum.AWS:
            AWS.move(src_url, dst_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            Azure.move(src_url, dst_url, config)

    @staticmethod
    def push_local(src_absolute_url_local, dst_url, is_absolute=False, config=None, storage=None, delete_local=True, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        dst_url = Storage.__normalize_path(dst_url, is_absolute, storage, config)

        src_files = [src_absolute_url_local]
        dst_urls = [dst_url]
        if Local.is_dir(src_absolute_url_local):
            files = Local.get_files(src_absolute_url_local, recursive=True)
            src_files = [os.path.join(src_absolute_url_local, file) for file in files]
            dst_urls = [os.path.join(dst_url, file) for file in files]

        for src_file, dst_url in zip(src_files, dst_urls):
            content = Local.read(src_file)
            if storage == cfg.StorageEnum.LOCAL:
                Local.write(dst_url, content)
            elif storage == cfg.StorageEnum.AWS:
                AWS.write(dst_url, content, config)
            elif storage == cfg.StorageEnum.AZURE:
                Azure.write(dst_url, content, config)

            if delete_local:
                Local.delete(src_file)    # remove the local version of file

    @staticmethod
    def pull_remote(src_url, dst_absolute_url_local, is_absolute=False, config=None, storage=None, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        src_url = Storage.__normalize_path(src_url, is_absolute, storage, config)
        if dst_absolute_url_local is None or dst_absolute_url_local == '':
            current_dir = os.path.dirname(os.path.realpath(__file__))
            dst_absolute_url_local = os.path.join(current_dir, os.path.basename(src_url))

        dir_ = os.path.dirname(dst_absolute_url_local)
        if not dir_:
            current_dir = os.path.dirname(os.path.realpath(__file__))
            dst_absolute_url_local = os.path.join(current_dir, dst_absolute_url_local)

        if storage == cfg.StorageEnum.LOCAL:
            if Local.file_exists(src_url):
                Local.write(dst_absolute_url_local, Local.read(src_url))
            else:
                src_files = Local.get_files(src_url)  # todo: recursive=True
                for src_file in src_files:
                    dst_file = os.path.join(dst_absolute_url_local, src_file)
                    os.makedirs(os.path.dirname(dst_file), exist_ok=True)
                    Local.write(dst_file, Local.read(os.path.join(src_url, src_file)))
        elif storage == cfg.StorageEnum.AWS:
            Local.write(dst_absolute_url_local, AWS.read(src_url, config))
        elif storage == cfg.StorageEnum.AZURE:
            if Azure.file_exists(src_url, config):
                Local.write(dst_absolute_url_local, Azure.read(src_url, config))
            else:
                src_files = Azure.get_files(src_url, config, recursive=True)
                for src_file in src_files:
                    dst_file = os.path.join(dst_absolute_url_local, src_file)
                    os.makedirs(os.path.dirname(dst_file), exist_ok=True)
                    Local.write(dst_file, Azure.read(os.path.join(src_url, src_file), None))
        elif storage == cfg.StorageEnum.NETWORK:
            Local.write(dst_absolute_url_local, Network.read(src_url, config))

    @staticmethod
    def get_absolute_path(file_url, from_base=False, config=None, storage=None, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        return Storage.__normalize_path(file_url, False, storage, config, from_base=from_base)

    @staticmethod
    def remove_prefix(text, prefix):
        if text.startswith(prefix):
            return text[len(prefix):]
        return text

    @staticmethod
    def get_relative_path(file_url, from_base=False, config=None, storage=None, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        iter_ = f'iteration_{Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.LEARNING_ITERATION)}'
        # relative_path = os.path.normpath(file_url).removeprefix(os.path.join(Storage.get_storage_prefix(storage), iter_))
        prefix = os.path.join(Storage.get_storage_prefix(storage), iter_)
        relative_path = Storage.remove_prefix(os.path.normpath(file_url), os.path.normpath(prefix))
        if relative_path == os.path.normpath(file_url):
            return None
        return relative_path[1:]

    @staticmethod
    def create_absolute_path(org, domain, sub_domain, session_name, model_type, iter_, file_url):
        store = Storage.get_storage(None, False).name.lower()
        folder = cfg.storage_cfg[store]["folder"]
        prefix = Zookeeper.get_session_prefix(org, domain, sub_domain, session_name, model_type)
        return os.path.normpath(os.path.join(folder, prefix, iter_, file_url))

    @staticmethod
    def get_external_path(file_url, is_absolute=False, from_base=False, config=None, storage=None, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        file_url = Storage.__normalize_path(file_url, is_absolute, storage, config, from_base=from_base)

        if storage == cfg.StorageEnum.LOCAL:
            return Local.get_absolute_path(file_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.get_absolute_path(file_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.get_external_path(file_url, config)

    @staticmethod
    def get_files(dir_url, is_absolute=False, config=None, storage=None, iteration=None, recursive=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        dir_url = Storage.__normalize_path(dir_url, is_absolute, storage, config, iteration=iteration) + '/'

        if storage == cfg.StorageEnum.LOCAL:
            return Local.get_files(dir_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.get_files(dir_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            ss = Azure.get_files(dir_url, config, recursive=recursive)
            return ss
        elif storage == cfg.StorageEnum.NETWORK:
            return Network.get_files(dir_url, config)

    @staticmethod
    def get_dirs(dir_url, is_absolute=False, config=None, storage=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        dir_url = Storage.__normalize_path(dir_url, is_absolute, storage, config, from_base) + '/'
        if storage == cfg.StorageEnum.LOCAL:
            return Local.get_dirs(dir_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.get_dirs(dir_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.get_dirs(dir_url, config)

    @staticmethod
    def get_model_iterations(only_previous=False):
        path = Storage.get_storage_prefix()
        iterations = list(Storage.get_dirs(path, from_base=True))
        if only_previous:
            current_iteration = f'iteration_{Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.LEARNING_ITERATION)}'
            iterations.remove(current_iteration)
        return iterations

    @staticmethod
    def clone_dir(src_dir, dst_dir, is_absolute=False, config=None, storage=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        src_dir = Storage.__normalize_path(src_dir, is_absolute, storage, config, from_base) + '/'
        dst_dir = Storage.__normalize_path(dst_dir, is_absolute, storage, config, from_base) + '/'
        if storage == cfg.StorageEnum.LOCAL:
            return Local.clone_dir(src_dir, dst_dir)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.clone_dir(src_dir, dst_dir, config)

    @staticmethod
    def delete_dir(dir_url, is_absolute=False, config=None, storage=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        dir_url = os.path.join(dir_url, '')
        dir_url = Storage.__normalize_path(dir_url, is_absolute, storage, config, from_base)
        if storage == cfg.StorageEnum.LOCAL:
            return Local.delete_dir(dir_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.get_dirs(dir_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.delete_dir(dir_url, config)

    @staticmethod
    def delete(dir_url, is_absolute=False, config=None, storage=None, from_base=False, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        #dir_url = os.path.join(dir_url, '')
        dir_url = Storage.__normalize_path(dir_url, is_absolute, storage, config, from_base)
        logger.info(f'dir_url: {dir_url}') 
        if storage == cfg.StorageEnum.LOCAL:
            return Local.delete(dir_url)
        elif storage == cfg.StorageEnum.AWS:
            return AWS.get_dirs(dir_url, config)
        elif storage == cfg.StorageEnum.AZURE:
            return Azure.delete(dir_url, config)

    @staticmethod
    def exec_command(command, config=None, storage=None, use_disk=False):
        storage = Storage.get_storage(storage, use_disk)
        if storage == cfg.StorageEnum.NETWORK:
            return Network.exec_command(command, config)

    @staticmethod
    def get_local_tmp_dir(module: ModuleEnum, sub_dir=''):
        return os.path.join(cfg.storage_cfg['local']['folder'], module.value, sub_dir)

    @staticmethod
    def get_file_path(session=None):
        config = cfg.storage_cfg
        storage = Storage.get_storage()
        store = 'azure' if storage == cfg.StorageEnum.AZURE else \
            'aws' if storage == cfg.StorageEnum.AWS else \
                'local' if storage == cfg.StorageEnum.LOCAL else \
                    'network' if storage == cfg.StorageEnum.NETWORK else ''
        folder = config[store]["folder"]
        session = session if session else Zookeeper.read(ModuleEnum.USER_INPUT, AttributeEnum.MODEL)
        return os.path.join(folder, zk_config.zk_root_path, session, 'zk_nodes.json')


class Helper:
    @staticmethod
    def get_golden_terms(reviewed: bool=True) -> list:
        if not reviewed:   # fetch non-reviewed terms aka terms
            terms = []
            path = os.path.join(Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.GOLDEN_TERMS), 'terms.gol')
            try:
                terms = list(map(eval, str(Storage.read(path), 'utf-8').splitlines()))
            except FileNotFoundError: pass
            return terms

        terms = set()
        iterations = Storage.get_model_iterations()    # fetch reviewed golden terms from all iterations
        for i in iterations:
            path = os.path.join(Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.GOLDEN_TERMS), 'input_terms.gol')
            try:
                terms = terms.union(str(Storage.read(path, iteration=i), 'utf-8').splitlines())
            except FileNotFoundError: pass
        return list(terms)

    @staticmethod
    def set_golden_terms(terms: list, reviewed: bool=True):
        terms_ = set(terms)
        file_name = 'input_terms.gol' if reviewed else 'terms.gol'
        path = os.path.join(Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.GOLDEN_TERMS), file_name)
        try:
            terms_ = terms_.union(str(Storage.read(path), 'utf-8').splitlines())
        except FileNotFoundError: pass
        Storage.write(path, bytes("\n".join(map(str, terms_)), encoding='utf-8'))

    @staticmethod
    def get_golden_doc_urls(model_id=None):
        document_links = []
        path = os.path.join(os.path.dirname(Storage.get_storage_prefix()), golden_documents)

        try:
            file_links = {}
            try:
                file_links = json.loads(Storage.read(path, is_absolute=True))
            except: pass
            if model_id == '':
                model_id = Zookeeper.read_path(f'/{zk_config.zk_deployment_path}/_model_id', append_root=False)
            for unparsed_link, info in file_links.items():
                m_id = info.get('model_id')
                if model_id is None or model_id == m_id:
                    external_link = info.get(GoldenLinkType.PUBLIC)
                    external_link = external_link if external_link else Storage.get_external_path(unparsed_link, is_absolute=True)
                    document_links.append((unparsed_link, external_link, m_id))
        except FileNotFoundError: pass
        return document_links

    @staticmethod
    def get_golden_doc_links(all_models=False) -> list:
        document_links = set()
        model_id = None
        if not all_models:
            model_id = Zookeeper.read_path(f'/{zk_config.zk_deployment_path}/_model_id', append_root=False)

        path = os.path.join(os.path.dirname(Storage.get_storage_prefix()), golden_documents)
        file_links = {}
        try:
            file_links = json.loads(Storage.read(path, is_absolute=True))
        except FileNotFoundError: pass

        for unparsed_link, info in file_links.items():
            if all_models or model_id in info.get('model_id'):
                document_links.add(unparsed_link)
        return list(document_links)

    @staticmethod
    def get_golden_doc_sentences(unparsed_links=None) -> list:
        document_links = []
        path = os.path.join(os.path.dirname(Storage.get_storage_prefix()), golden_documents)
        registered_links = {}
        try:
            registered_links = json.loads(Storage.read(path, is_absolute=True))
        except FileNotFoundError: pass

        unparsed_links = registered_links.keys() if unparsed_links is None else unparsed_links
        for unparsed_link in unparsed_links:
            info = registered_links.get(unparsed_link, {})
            sentence_link = info.get(GoldenLinkType.SENTENCES)
            document_links.append(sentence_link)
        return document_links

    @staticmethod
    def get_golden_doc_paragraphs(unparsed_links=None) -> list:
        document_links = []
        path = os.path.join(os.path.dirname(Storage.get_storage_prefix()), golden_documents)
        registered_links = {}
        try:
            registered_links = json.loads(Storage.read(path, is_absolute=True))
        except FileNotFoundError: pass
        
        unparsed_links = registered_links.keys() if unparsed_links is None else unparsed_links
        for unparsed_link in unparsed_links:
            info = registered_links.get(unparsed_link, {})
            paragraph_link = info.get(GoldenLinkType.PARAGRAPHS)
            document_links.append(paragraph_link)
        return document_links

    @staticmethod
    def get_golden_doc_concepts(unparsed_links=None) -> list:
        document_links = []
        path = os.path.join(os.path.dirname(Storage.get_storage_prefix()), golden_documents)
        registered_links = {}
        try:
            registered_links = json.loads(Storage.read(path, is_absolute=True))
        except FileNotFoundError: pass

        unparsed_links = registered_links.keys() if unparsed_links is None else unparsed_links
        for unparsed_link in unparsed_links:
            info = registered_links.get(unparsed_link, {})
            concept_link = info.get(GoldenLinkType.CONCEPTS)
            document_links.append(concept_link)
        return document_links

    @staticmethod
    def get_golden_doc_triples(unparsed_links=None) -> list:
        document_links = []
        path = os.path.join(os.path.dirname(Storage.get_storage_prefix()), golden_documents)
        registered_links = {}
        try:
            registered_links = json.loads(Storage.read(path, is_absolute=True))
        except FileNotFoundError: pass

        unparsed_links = registered_links.keys() if unparsed_links is None else unparsed_links
        for unparsed_link in unparsed_links:
            info = registered_links.get(unparsed_link, {})
            triple_link = info.get(GoldenLinkType.TRIPLES)
            document_links.append(triple_link)
        return document_links

    @staticmethod
    def set_golden_doc_links(links: dict, link_type=GoldenLinkType.PUBLIC):
        path = os.path.join(os.path.dirname(Storage.get_storage_prefix()), golden_documents)
        file_links = {}
        try:
            file_links = json.loads(Storage.read(path, is_absolute=True))
        except FileNotFoundError: pass

        model_id = Zookeeper.read_path(f'/{zk_config.zk_deployment_path}/_model_id', append_root=False)
        for unparsed_link, link_to_add in links.items():
            info = file_links.get(unparsed_link, {})
            models = set(info.get('model_id', []))
            models.add(model_id)
            info['model_id'] = list(models)
            if link_type != GoldenLinkType.UNPARSED:
                info[link_type] = link_to_add
            file_links[unparsed_link] = info
        Storage.write(path, bytes(json.dumps(file_links), encoding='utf-8'), is_absolute=True)

    @staticmethod
    def set_supervised_model(name, description):
        path = os.path.join(os.path.dirname(Storage.get_storage_prefix()), supervised_model_mapping_file)
        models = {}
        try:
            models = json.loads(Storage.read(path, is_absolute=True))
        except: pass
        model = models.get(name, {})

        model['description'] = description if description else ''
        model['version'] = str(float(model.get('version', '0.0'))+1)
        all_versions = model.get('models', {})
        all_versions[model['version']] = Zookeeper.read_path(f'/{zk_config.zk_deployment_path}/_model_id', append_root=False)
        model['models'] = all_versions

        models[name] = model
        Storage.write(path, bytes(json.dumps(models), encoding='utf-8'), is_absolute=True)

    @staticmethod
    def get_model_links(model_path_prefix=None, model_id=None, model_names=None) -> dict:
        if not model_path_prefix:
            model_path_prefix = os.path.dirname(Storage.get_storage_prefix())
        model_mapping = json.loads(Storage.read(os.path.join(model_path_prefix, model_mapping_file), is_absolute=True))

        models = {}
        if model_id:
            _, _, _, m = model_mapping[model_id]
            if model_names:
                for model_name in model_names:
                    m_path = m.get(model_name)
                    if m_path:
                        models[model_name] = m_path
            else:
                models = m
        else:
            if model_names:
                iter_ = 0
                for _, i, n, m in model_mapping.values():
                    i = int(next(filter(str.isdigit, i)))
                    for m_name, m_path in m.items():
                        if m_name in model_names and (m_name not in models or i > iter_):
                            iter_ = i
                            models[m_name] = m_path
            else:
                iter_ = {}   # keep model_name vs iteration number
                for _, i, n, m in model_mapping.values():
                    i = int(next(filter(str.isdigit, i)))
                    for m_name, m_path in m.items():
                        if m_name not in models or i > iter_[m_name]:
                            iter_[m_name] = i
                            models[m_name] = m_path
        return models

    @staticmethod
    def set_model_mapping(model_name: ModelEnum, model_absolute_path):
        model_id = Zookeeper.read_path(f'/{zk_config.zk_deployment_path}/_model_id', append_root=False)
        path = os.path.join(os.path.dirname(Storage.get_storage_prefix()), model_mapping_file)
        model_mapping = json.loads(Storage.read(path, is_absolute=True))

        model_type, iter_, m_name, models = model_mapping[model_id]
        models[model_name] = model_absolute_path
        model_mapping[model_id] = (model_type, iter_, m_name, models)
        Storage.write(path, bytes(json.dumps(model_mapping, default=str), 'utf-8'), is_absolute=True)


class HelperEx:
    @staticmethod
    def get_golden_doc_urls(org, domain, subdomain, sess):
        path = os.path.join(f'{Storage.get_base_prefix()}/{org}/{domain}/{subdomain}/{sess}', golden_documents)
        file_links = {}
        try:
            file_links = json.loads(Storage.read(path, is_absolute=True))
        except FileNotFoundError: pass

        document_links = []
        for unparsed_link, info in file_links.items():
            external_link = info.get(GoldenLinkType.PUBLIC)
            external_link = external_link if external_link else Storage.get_external_path(unparsed_link, is_absolute=True)
            document_links.append((unparsed_link, external_link))
        return document_links

    @staticmethod
    def get_selected_golden_docs(org, domain, subdomain, sess, context_name):
        prefix = f'{Storage.get_base_prefix()}/{org}/{domain}/{subdomain}/{sess}'
        context_mapping = {}
        try:
            context_mapping = json.loads(Storage.read(os.path.join(prefix, supervised_model_mapping_file), is_absolute=True))
        except: pass
        contexts = [(name, context.get('description'), context.get('version'), context.get('models')) for name, context in context_mapping.items()]

        context_models = []
        for name, desc, ver, model_ids in contexts:
            if context_name == name:
                context_models = model_ids.values()
                break

        selected_docs = set()
        for model_id in context_models:
            try:
                model_mapping = json.loads(Storage.read(os.path.join(prefix, model_mapping_file), is_absolute=True))
                model_type, iter_, _, _ = tuple(model_mapping[model_id])
                url = os.path.join(ModuleEnum.USER_INPUT.value, 'golden_document_links.txt')
                model_path = Storage.create_absolute_path(org, domain, sub_domain, session_name, model_type, iter_, url)
                docs = str(Storage.read(model_path, is_absolute=True), 'utf-8').splitlines()
                selected_docs |= set(docs)
            except: pass
        return selected_docs

    @staticmethod
    def get_supervised_models(org, domain, subdomain, sess):
        prefix = f'{Storage.get_base_prefix()}/{org}/{domain}/{subdomain}/{sess}'
        models = {}
        try:
            models = json.loads(Storage.read(os.path.join(prefix, supervised_model_mapping_file), is_absolute=True))
        except: pass
        return [(name, details.get('description'), details.get('version'), details.get('models')) for name, details in models.items()]

    @staticmethod
    def set_golden_terms(org, domain, subdomain, sess, model_type, iter_, terms):
        prefix = f'{Storage.get_base_prefix()}/{org}/{domain}/{subdomain}/{sess}/{model_type}/{iter_}'
        path = os.path.join(prefix, Zookeeper.read(ModuleEnum.CONFIG, AttributeEnum.GOLDEN_TERMS), reviewed_golden_terms)

        terms_ = set(terms)
        try:
            terms_ = terms_.union(str(Storage.read(path, is_absolute=True), 'utf-8').splitlines())
        except FileNotFoundError: pass
        Storage.write(path, bytes("\n".join(map(str, terms_)), encoding='utf-8'), is_absolute=True)

    @staticmethod
    def set_golden_doc_links(org, domain, subdomain, sess, model_id, links: dict):
        prefix = f'{Storage.get_base_prefix()}/{org}/{domain}/{subdomain}/{sess}'
        link_type = GoldenLinkType.PUBLIC
        path = os.path.join(prefix, golden_documents)
        file_links = {}
        try:
            file_links = json.loads(Storage.read(path, is_absolute=True))
        except: pass
        for unparsed_link, link_to_add in links.items():
            info = file_links.get(unparsed_link, {})
            models = set(info.get('model_id', []))
            models.add(model_id)
            info['model_id'] = list(models)
            info[link_type] = link_to_add
            file_links[unparsed_link] = info
        Storage.write(path, bytes(json.dumps(file_links), encoding='utf-8'), is_absolute=True)
