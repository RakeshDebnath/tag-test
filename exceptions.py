// src/config/config.js

const config = {
  train_api_ip: '129.80.129.149',
  train_api_port: '8585',

  login_api_ip: 'auth.parabole.ai',
  login_api_port: '5010',

  // hyp_vld_api_ip: '129.80.129.149', // new apm server
  hyp_vld_api_ip: '132.145.210.179', // new apm server + documents
  // hyp_vld_api_ip: '129.80.139.145', //bp server
  // hyp_vld_api_ip: '129.213.19.33', // fti-sci
  
  hyp_vld_api_port: '5303', // for new train runtime 
  // hyp_vld_api_port: '5302',
  
  admin_email : 'admin@gmail.com',

}; 


export default config;