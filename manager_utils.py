from functools import partial, wraps
import time
import logging
import logging_util
logger = logging.getLogger(__name__)


def retry(func=None, exception=Exception, tries=5, delay=5, delay_factor=1):
    if func is None:
        return partial(retry, exception=exception, tries=tries, delay=delay, delay_factor=delay_factor)

    @wraps(func)
    def wrapper(*args, **kwargs):
        n_tries, n_delay = tries, delay
        while n_tries > 1:
            try:
                return func(*args, **kwargs)
            except exception as e:
                logger.warning(f"{str(e)}, Retrying in {n_delay} seconds...")
                time.sleep(n_delay)
                n_tries -= 1
                n_delay *= delay_factor

        return func(*args, **kwargs)

    return wrapper
