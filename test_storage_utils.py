import time
import random
from storage_utils import *
from storage_config import *
import datetime
import os
from multiprocessing import Process, current_process

test_dir = 'test_dir/test1'


class Test(Process):

    @staticmethod
    def write_data():
        print(f'{current_process().name} invoked')
        random_time = random.randint(4, 10)
        print(f'Wait for {random_time} seconds for no good reason.')
        time.sleep(random_time)

        with open('test/sample.txt', 'r') as file:
            content = file.read()
        Storage.write(test_dir, bytes(content, encoding='utf-8'))

    @staticmethod
    def read_data():
        content = Storage.read(test_dir)
        print(content.decode('utf-8'))

    def run(self):
        #self.write_data() # First try to write something to test the Local.write() method.
        #time.sleep(10) # Have patience my friend.
        self.read_data() # Now, try to read the already written stuff to test the Local.read() method.


if __name__=='__main__':
    Test().start()
    while True:
        print(f'{current_process().name} running')
        time.sleep(3)
"""
    config = {
        'network': {
            'host_name': '172.16.214.22',
            'user_name': 'parabole',
            'password': 'Dur01nag',
        }
    }
    dir_url = '/home/parabole/automation/learning/downloader/Partnerships_for_the_Goals'
    for file in Storage.get_files(dir_url, True, config, StorageEnum.NETWORK):
        Storage.pull_remote(os.path.join(dir_url, file), f'test1/{file}', True, config, StorageEnum.NETWORK)

"""